; Vars
global restDelay:="180000" ; Time to recover health reserves
global runDessslay:="60000" ; Suicide run length, depens on char speed
global priestButCoordX:="349"
global priestButCoordY:="659"
global timesBetweenRest:="5"


resurrect()
{
    ; Turn to priest, resurrect and turn back
    SetKeyDelay, 10, 200
    ControlSend,, {Tab} , ahk_class UnrealWindow

    SetKeyDelay, 10, 1200
    ControlSend,, {Right}, ahk_class UnrealWindow


    SetKeyDelay, 10, 2000
    ControlSend,, r, ahk_class UnrealWindow
    sleep, 5000


    SetKeyDelay, 10, 1100
    ControlSend,, {Left}, ahk_class UnrealWindow
    
    return
}

becomeAnHero()
{
    ; Run to the edge, die, teleport back to priest
    SetKeyDelay, 10, 60000
    ControlSend,, w, ahk_class UnrealWindow
    sleep, 2000
    SetKeyDelay, 10, 200
    ControlSend,, {Tab} , ahk_class UnrealWindow
    sleep, 2000     

    ; Sometimes it is bugging, clicking 2 times
    loop 2
    {
        MouseMove, %priestButCoordX%, %priestButCoordY%
        SetKeyDelay 100
        ControlClick, x%priestButCoordX% y%priestButCoordY%, ahk_class UnrealWindow,,Left
    }
    sleep,25000
    return
}

rest()
{
    SetKeyDelay, 10, 100
    ControlSend,, 8 , ahk_class UnrealWindow
    sleep, %restDelay%
    SetKeyDelay, 10, 100
}

F1::
loop {   
    resurrect() 
    rest()
    becomeAnHero()
    loop %timesBetweenRest%
    {
        resurrect()
        becomeAnHero()
    }   

}

